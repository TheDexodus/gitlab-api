<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\TimeSpent;
use DateTime;

final class TimeReport
{
    private Api $api;

    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    public function getCollectDaysReport(int $project, string $user): array
    {
        $notes = [];
        $issues = $this->api->getIssuesByProject($project);

        foreach ($issues as $issue) {
            if ($issue['time_stats']['total_time_spent'] === 0) {
                continue;
            }

            $notes = array_merge($notes, $this->api->getTimeSpentNotes(
                $project,
                $issue['iid'],
                $issue['time_stats']['total_time_spent'],
                $user,
            ));
        }

        return $this->collectByDays($notes);
    }

    public function getCollectYearsReport(int $project, string $user): array
    {
        $notes = [];
        $issues = $this->api->getIssuesByProject($project);
        foreach ($issues as $issue) {
            if ($issue['time_stats']['total_time_spent'] === 0) {
                continue;
            }

            $notes = array_merge($notes, $this->api->getTimeSpentNotes(
                $project,
                $issue['iid'],
                $issue['time_stats']['total_time_spent'],
                $user,
            ));
        }

        return $this->collectByYears($notes);
    }

    private function collectByDays(array $notes): array
    {
        $reports = [];

        /** @var TimeSpent $note */
        foreach ($notes as $note) {
            if (!isset($reports[$note->getDate()])) {
                $reports[$note->getDate()] = ['date' => $note->getDate(), 'minutes' => $note->getDuration()];
            } else {
                $reports[$note->getDate()]['minutes'] += $note->getDuration();
            }
        }

        ksort($reports);

        return array_map(static function (array $report): array {
            $time = $report['minutes'] >= 0 ? '' : '-';
            $time .= sprintf('%dh', (int)($report['minutes'] / 60));
            $time .= sprintf(' %dm', (int)($report['minutes'] % 60));

            return ['date' => $report['date'], 'minutes' => $report['minutes'], 'time' => $time];
        }, $reports);
    }

    private function collectByYears(array $notes): array
    {
        $reportsCollectedByDays = $this->collectByDays($notes);
        $reportsCollectedByYears = [];

        foreach ($reportsCollectedByDays as $report) {
            $date = DateTime::createFromFormat('Y-m-d', $report['date']);
            $yearString = $date->format('Y');
            $monthString = $date->format('M(m)');

            if (!isset($reportsCollectedByYears[$yearString])) {
                $reportsCollectedByYears[$yearString] = [
                    'minutes' => 0,
                    'months' => [],
                ];
            }

            if (!isset($reportsCollectedByYears[$yearString]['months'][$monthString])) {
                $reportsCollectedByYears[$yearString]['months'][$monthString] = [
                  'minutes' => 0,
                  'reports' => [],
                ];
            }

            $reportsCollectedByYears[$yearString]['minutes'] += $report['minutes'];
            $reportsCollectedByYears[$yearString]['months'][$monthString]['minutes'] += $report['minutes'];
            $reportsCollectedByYears[$yearString]['months'][$monthString]['reports'][] = $report;
        }

        return $reportsCollectedByYears;
    }
}
