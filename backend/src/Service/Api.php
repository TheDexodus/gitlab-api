<?php

declare(strict_types=1);

namespace App\Service;

use App\Model\TimeSpent;
use Gitlab\Client;
use Gitlab\ResultPager;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

final class Api
{
    private Client      $client;
    private ResultPager $pager;

    public function __construct(Client $client, ResultPager $pager)
    {
        $this->client = $client;
        $this->pager = $pager;
    }

    public function getIssuesByProject(int $project): array
    {
        return $this->pager->fetchAll($this->client->issues(), 'all', [$project]);
    }

    public function getTimeSpentNotes(int $project, int $issue, int $totalTimeSpent, string $user): array
    {
        $cache = new FilesystemAdapter();
        $itemKey = $issue . '.' . $user . '.' . $totalTimeSpent;
        $timeSpentNotesItem = $cache->getItem($itemKey);

        if (!$timeSpentNotesItem->isHit()) {
            $timeSpentNotes = [];
            $notes = $this->getNotes($project, $issue);
            foreach ($notes as $note) {
                if ($user !== $note['author']['username']) {
                    continue;
                }

                if ('removed time spent' === $note['body']) {
                    $timeSpentNotes = [];
                } elseif (str_contains($note['body'], 'time spent')) {
                    $timeSpentNotes[] = $note;
                }
            }

            $timeSpentNotesItem->set(array_map([self::class, 'getDurationInfo'], $timeSpentNotes));
            $cache->save($timeSpentNotesItem);
        }

        return $cache->getItem($itemKey)->get();
    }

    public function getNotes(int $project, int $issue): array
    {
        return $this->pager->fetchAll(
            $this->client->issues(),
            'showNotes',
            [
                $project,
                $issue,
                [
                    'order_by' => 'created_at',
                    'sort'     => 'asc',
                ],
            ],
        );
    }

    public static function getDurationInfo(array $note): TimeSpent
    {
        $coefficients = ['m' => 1, 'h' => 60];
        preg_match('~^(?:added|subtracted) (.*) of time spent~', $note['body'], $matchesDuration);

        if (!isset($matchesDuration[1])) {
            return new TimeSpent(date('Y-m-d'), false, 0);
        }

        $duration = 0;
        foreach (explode(' ', $matchesDuration[1]) as $chunk) {
            $duration += $coefficients[$chunk[-1]] * (int) $chunk;
        }

        $negative = str_contains($matchesDuration[0], 'subtracted');
        preg_match('~\d{4}-\d{2}-\d{2}$~', $note['body'], $matchesDate);
        preg_match('~\d{4}-\d{2}-\d{2}~', $note['created_at'], $matchesDate2);

        $date = $matchesDate2[0];

        if (isset($matchesDate[0])) {
            $date = $matchesDate[0];
        }

        return new TimeSpent($date, $negative, $duration);
    }
}
