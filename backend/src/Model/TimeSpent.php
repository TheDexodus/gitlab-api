<?php

declare(strict_types=1);

namespace App\Model;

final class TimeSpent
{
    private string $date;
    private bool $negative;
    private int $minutes;

    public function __construct(string $date, bool $negative, int $minutes)
    {
        $this->date = $date;
        $this->negative = $negative;
        $this->minutes = $minutes;
    }

    public function getDate(): string
    {
        return $this->date;
    }

    public function isNegative(): bool
    {
        return $this->negative;
    }

    public function getMinutes(): int
    {
        return $this->minutes;
    }

    public function getDuration(): int
    {
        return $this->isNegative() ? -$this->getMinutes() : $this->getMinutes();
    }
}
