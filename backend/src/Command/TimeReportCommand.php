<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\TimeReport;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class TimeReportCommand extends Command
{
    protected static   $defaultName = 'app:time-report';

    private TimeReport $timeReport;

    public function __construct(TimeReport $timeReport)
    {
        parent::__construct();

        $this->timeReport = $timeReport;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('project', InputArgument::REQUIRED, 'The gitlab project.')
            ->addArgument('user', InputArgument::REQUIRED, 'The gitlab user.')
            ->addArgument('payday', InputArgument::REQUIRED, 'Payday per 1 hour.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $report = $this->timeReport->getCollectYearsReport(
            (int) $input->getArgument('project'),
            $input->getArgument('user')
        );

        $payday = (float)$input->getArgument('payday');

        $allMinutes = 0;

        foreach ($report as $year => $item) {
            $allMinutes += $item['minutes'];
            $yearHours = round($item['minutes'] / 60, 2);
            $output->writeln(sprintf('%s [%s hours, earned %s$]:', $year, $yearHours, $yearHours * $payday));

            foreach ($item['months'] as $month => $monthReports) {
                $monthHours = round($monthReports['minutes'] / 60, 2);
                $output->writeln(sprintf('   %s [%s hours, earned %s$]', $month, $monthHours, $monthHours * $payday));

                foreach ($monthReports['reports'] as $item) {
                    $dayHours = round($item['minutes'] / 60, 2);
                    $output->writeln(sprintf('      %s - %s, earned %s$', $item['date'], $item['time'], $dayHours * $payday));
                }
            }
        }


        $allHours = round($allMinutes / 60, 2);
        $output->writeln('');
        $output->writeln(sprintf('All time you worked %s hours and earned %s$', $allHours, $allHours * $payday));

        return Command::SUCCESS;
    }
}
